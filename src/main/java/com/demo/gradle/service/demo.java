package com.demo.gradle.service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/")

public class demo {

    @GetMapping
    public String start(){
        return "Hello world from Gradle!!!";
    }
}
